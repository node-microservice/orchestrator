import * as os from 'os'
import * as minimist from 'minimist'
import { MicroService, RouteCallback, HttpMessageBuilder, HttpMessage, WebSocketManager, MatchOptions, HttpMethod } from 'microservice'
import { HEARTBEAT_PATH, REGISTER_PATH, RegisterOptions } from './lib'

interface Service {
    id: number
    name: string
    path: string
    method: HttpMethod
    remotePath: string
    status_last?: string
    status_timestamp?: Date
    timestamp: Date
    conn: WebSocketManager
}

// Main entrypoint
export async function main(opts: any = {}): Promise<MicroService> {

    // Parse command-line arguments
    const args = minimist(process.argv.slice(1))

    // Helper function used to read arguments. The order of preference is: 1) opts 2) args 3) env
    function getopt(name: string, env = true): string {
        return opts[name] || args[name] || (env && process.env[name])
    }

    // Parse command-line arguments and define parameters
    const GLOBAL_STATS: {[key: string]: any} = {}
    const MAX_STATUS_RETRY = 5
    let REQUESTS_TOTAL = 0
    let REQUESTS_SUCCESS = 0
    let REQUESTS_FAIL = 0

    // Initialize microservice. Cache expiration must be very short to allow debouncing while not
    // preventing registration of many services.
    const orchestrator = await MicroService.create(getopt('db'), 5, 1)

    // Setup debugging
    process.on('unhandledRejection', err => orchestrator.log('E', err))

    // Keep an index of the last triggered service for each path to allow round-robin selection
    const serviceIndex = new Map<string, number>()
    const serviceRoutes = new Map<string, MatchOptions>()
    const serviceDirectory = new Map<string, Service[]>()

    // Helper function to request a status from a single service given the status path
    function serviceStatus(service: Service, retried = 0): Promise<HttpMessage> {
        return new Promise<any>(async (resolve, reject) => {
            const query = new URLSearchParams()
            query.set('date', new Date().getTime().toString())
            query.set('name', service.name)
            query.set('path', service.path)
            try {
                await orchestrator.log('I', `Requesting status for service ${service.conn.id}`)
                const request = new HttpMessageBuilder()
                    .setMethod('GET')
                    .setPath(`${HEARTBEAT_PATH}?${query}`)
                    .build()
                const status = await service.conn.request(request)
                const msg = `Service ${service.name}${service.remotePath} responded with status: ` +
                    JSON.stringify(status.json)
                await orchestrator.log('V', msg)
                await orchestrator.db.run(`
                    UPDATE services SET status_last=(?), status_timestamp=datetime('now')
                    WHERE id=(?)`, [status.body, service.id])
                resolve(status)

            } catch (err) {
                const msg = await orchestrator.log('E',
                    `Error querying service ${service.name} for status:`, err.message)
                if (retried < MAX_STATUS_RETRY) {
                    setTimeout(() => {
                        serviceStatus(service, retried++).then(resolve).catch(reject)
                    }, 1 * 60 * 1000)  // wait a minute before retry
                } else if (err.code === 'ECONNREFUSED') {
                    reject(new Error(msg))
                } else {
                    reject(new Error(msg))
                }
            }
        })
    }

    /**
     * Helper function used to request a status from all services and delete stale ones
     */
    async function cleanupServices(): Promise<void> {
        await Promise.all(Array.from(serviceDirectory.values()).map(async services => {
            await Promise.all(services.map(async service => {
                await serviceStatus(service).catch(err => deleteService(service))
            }))
        }))
    }

    async function deleteService(service: Service) {

        // Try to close the connection in case it is still alive
        try {
            service.conn.close()
        } catch (exc) {
            orchestrator.log('E', 'Error closing connection for service being deleted', exc.message)
        }

        // Find the service within out directory
        const key = serviceKey(service)
        const services = serviceDirectory.get(key)

        // Delete service from directory
        const idx = services.indexOf(service)
        if (idx !== -1) services.splice(idx, 1)
        serviceDirectory.set(key, services)

        // If no services are left for this name, delete route as well
        if (services.length === 0) {
            orchestrator.unroute(serviceRoutes.get(key))
            serviceRoutes.delete(key)
            serviceDirectory.delete(key)
            orchestrator.log('V', `Deleted route for service: ${service.name}${service.remotePath}`)
        }

        // Always restart the indexing
        serviceIndex.delete(key)

        // Update database
        await orchestrator.db.run(`DELETE FROM services WHERE id = ?`, [service.id])
    }

    // Initialize services database
    orchestrator.db.run(`
        CREATE TABLE IF NOT EXISTS services (
            id                TEXT NOT NULL,
            name              TEXT NOT NULL,
            path              TEXT NOT NULL,
            remote_path       TEXT NOT NULL,
            method            TEXT NOT NULL,
            active            INT DEFAULT 1,
            status_last       TEXT DEFAULT NULL,
            status_timestamp  DATETIME DEFAULT CURRENT_TIMESTAMP,
            timestamp         DATETIME DEFAULT CURRENT_TIMESTAMP,
            UNIQUE (id, name, path))`)

    // Routes setup
    orchestrator.route({method: 'POST', path: REGISTER_PATH}, async (request, _, conn) => {
        // Retrieve the specifications from body
        const spec = await request.json as RegisterOptions

        // Make sure that the request was done via WebSocket
        if (!conn) {
            return new HttpMessageBuilder()
                .setStatusCode(400).setBody('Please use WebSocket for registration').build()
        }

        // Create Service object from each route in specification object received
        const services = spec.routes.map(route => Object.assign(
            {id: conn.id, name: spec.name, conn: conn, timestamp: new Date()}, route) as Service)

        // Register each service, cancel if one or more fail
        try {
            for (const service of services) {
                await registerService(service)
            }
            return `${services.length} services registered successfully`
        } catch (exc) {
            for (const service of services) {
                await deleteService(service)
            }
            return new HttpMessageBuilder().setStatusCode(500).setBody(exc)
        }
    })

    function serviceKey(service: Service) {
        return `${service.method}${service.name}${service.remotePath}`
    }

    async function registerService(service: Service) {

        // Query status for the service before adding route
        try {
            service.status_last = (await serviceStatus(service)).body.toString('UTF-8')

            // Add service to directory
            const key = serviceKey(service)
            const services = (serviceDirectory.get(key) || []).concat([service])
            serviceDirectory.set(key, services)

            // When the connection to this service dies, we remove it from our directory
            service.conn.socket.on('close', () => {
                orchestrator.log('V', `Connection with service closed: ${service.name} (${service.id})`)
                deleteService(service)
            })

            // Add route for this service only if first
            if (services.length === 1) {
                const route = orchestrator.route(
                    {method: service.method, path: `/service/${service.name}${service.remotePath}`},
                    serviceHandlerFactory(service))
                serviceRoutes.set(key, route)
            }

            // Add service to database
            await orchestrator.db.run(`
                INSERT INTO services (id, name, path, remote_path, method, status_last)
                VALUES (?, ?, ?, ?, ?, ?)`,
                [service.id, service.name, service.path, service.remotePath,
                 service.method, service.status_last])

            const serviceFrom = `[${service.method}] ${service.name}${service.path}`
            const serviceTo = `${service.id}${service.remotePath}`
            await orchestrator.log('I', `Service registered successfully: ` +
                `${serviceFrom} => ${serviceTo}`)

        } catch (err) {
            orchestrator.log('E', err.message)
            const msg = await orchestrator.log('E',
                `Error occurred when registering service "${service.name}"`)
            throw new Error(msg)
        }
    }

    orchestrator.route('/status', async request => {
        try {
            const rows = await orchestrator.db.all(`
                SELECT id, name, path, remote_path, method, status_last, status_timestamp
                FROM services WHERE active = 1`)
            rows.forEach((row: any) => {
                try {
                    row.status_last = JSON.parse(row.status_last)
                } catch (ex) {
                    // No-op
                }
            })
            GLOBAL_STATS.services = rows
            return GLOBAL_STATS
        } catch (err) {
            orchestrator.log('E', err.message)
            const msg = await orchestrator.log('E', 'Unable to retrieve status from database')
            return new HttpMessageBuilder().setStatusCode(500).setBody(msg)
        }
    })

    // Used for any http method request handling
    function serviceHandlerFactory(service: Service): RouteCallback {
        return async function(request, params) {
            REQUESTS_TOTAL++
            const key = serviceKey(service)
            const services = serviceDirectory.get(key)
            if (!serviceIndex.has(key)) {
                serviceIndex.set(key, 0)
            }
            const index = serviceIndex.get(key)
            const remoteService = services ? services[index] : null

            try {
                if (!remoteService)
                    throw ({code: 'SERVICE_NOT_FOUND', message: `Service not found: ${service}`})
                serviceIndex.set(key, (index + 1) % services.length)

                // Replace local path with remote path (but keep the rest of the path components)
                const url = new URL(`http://localhost${request.path}`)
                let path = remoteService.path + url.search
                Object.keys(params).forEach(key => path = path.replace(`:${key}`, params[key]))
                const ididx = `(${remoteService.id}) [${index + 1}/${services.length}]`
                orchestrator.log('V', `Sending ${request.path} to ${path} ${ididx}`)

                // Get data from remote service by pushing the request to the connection
                const redirect = new HttpMessageBuilder(request).setPath(path).build()
                const result = await remoteService.conn.request(redirect)

                // Mark as success and return the response from remote server
                REQUESTS_SUCCESS++
                return result

            } catch (err) {
                REQUESTS_FAIL++

                // If we can't connect to the service, attempt to unregister immediately
                if (err.code === 'ECONNREFUSED') {
                    await deleteService(remoteService)
                    const msg = await orchestrator.log(
                        'E', 'Service refused to connect: ' + err.message)
                    return new HttpMessageBuilder().setStatusCode(500).setBody(msg)

                } else {
                    const msg = await orchestrator.log('E', 'Error requesting service:', err)
                    return new HttpMessageBuilder().setStatusCode(500).setBody(msg)
                }
            }
        }
    }

    // Maintenance loop
    const maintenance_loop = async () => {

        const requests_total = REQUESTS_TOTAL
        GLOBAL_STATS.requests_per_second = 
            (requests_total - (GLOBAL_STATS.requests_last || 0)) / 60.0
        GLOBAL_STATS.requests_last = requests_total
        const error_rows = await orchestrator.db.all(`
            SELECT timestamp, message FROM log WHERE level = 'E' ORDER BY timestamp DESC LIMIT 10`)
        const error_msgs = error_rows.map((row: any) => row.message)
        GLOBAL_STATS.last_errors = error_rows
            .filter((row: any, ix) => error_msgs.indexOf(row.message) === ix)
            .map((row: any) => `[${row.timestamp}] ${row.message}`)
        GLOBAL_STATS.loadavg = os.loadavg()

        await cleanupServices()
    }
    setInterval(maintenance_loop, parseInt(getopt('maintenance')) || 60000)

    // Pick a random port
    const port = parseInt(getopt('port')) || Math.floor(Math.random() * 8975) + 1024
    orchestrator.start(port)

    // Notify other components of startup, but do not exit on failure
    await orchestrator.notify().catch(err => { /* already logged by notify() */ })

    return orchestrator
}

// If starting from the command line, begin execution
if (typeof require !== 'undefined' && require.main === module) {
    main()
}
