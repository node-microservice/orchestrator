import { MicroService, HttpRequest, HttpMethod, WebSocketRequest, HttpMessageBuilder } from 'microservice'
import * as assert from 'assert'
import { main } from '../index'
import { register, HEARTBEAT_PATH, RegisterOptions } from '../lib'

describe('orquestrator', function() {
    let orchestrator: MicroService
    let request: () => HttpRequest | WebSocketRequest
    const port_orchestrator = Math.floor(Math.random() * 8975) + 1024

    before(async () => {
        orchestrator = await main()
    })

    beforeEach(async () => {
        orchestrator.start(port_orchestrator)
        request = () => new WebSocketRequest(orchestrator.server)
    })
    afterEach(() => {
        orchestrator.stop()
        orchestrator.clearCache(true)
    })

    /**
     * Helper function used to create a new service using MicroService framework
     */
    async function newService() {
        const service = await MicroService.create()
        const port_service = Math.floor(Math.random() * 8975) + 1024
        service.start(port_service)
        return service
    }

    describe('heartbeat', () => {

        it('OK', async () => {
            const service = await newService()

            // Setup routes to fake a service
            const name = 'fakeservice'
            const path = '/' + Date.now()
            service.route(path, _ => path.substring(1))

            // Register the route as a service
            const url_self = 'http://127.0.0.1:' + port_orchestrator
            await register(
                service, {url: new URL(url_self), name: name, routes: [{path: path}]})

            // Test service heartbeat
            const res = await new HttpRequest(service.server).get(`${HEARTBEAT_PATH}`)
            assert.equal(res.statusCode, 200)

            service.stop()
        })
    })

    describe('register', () => {

        it('random path', async () => {
            const service = await newService()

            // Setup routes to fake a service
            const name = 'fakeservice'
            const path = '/' + Date.now()
            service.route(path, _ => path.substring(1))

            // Register the route as a service
            const url_self = 'http://127.0.0.1:' + port_orchestrator
            await register(
                service, {url: new URL(url_self), name: name, routes: [{path: path}]})

            // Test service result
            const res = await request().get(`/service/${name}${path}`)
            assert.equal(res.statusCode, 200)
            assert.equal(res.body, path.substr(1))

            service.stop()
        })

        it('same path twice', async () => {
            const service = await newService()

            // Setup routes to fake a service
            const name = 'fakeservice'
            const path = '/' + Date.now()
            const opts = service.route(path, _ => path.substring(1))

            // Register the route as a service
            const url_self = 'http://127.0.0.1:' + port_orchestrator
            await register(
                service, {url: new URL(url_self), name: name, routes: [{path: path}]})
            await register(
                service, {url: new URL(url_self), name: name, routes: [{path: path}]})

            // Test service result
            const res = await request().get(`/service/${name}${path}`)
            assert.equal(res.statusCode, 200)
            assert.equal(res.body, path.substr(1))

            service.stop()
        })

        it('root path', async () => {
            const service = await newService()

            // Setup routes to fake a service
            const name = 'fakeservice'
            const path = '/'
            service.route(path, _ => path.substring(1))

            // Register the route as a service
            const url_self = 'http://127.0.0.1:' + port_orchestrator
            await register(
                service, {url: new URL(url_self), name: name, routes: [{path: path}]})

            // Test service result
            const res = await request().get(`/service/${name}${path}?${Date.now()}`)
            assert.equal(res.statusCode, 200)
            assert.equal(res.body, path.substr(1))

            service.stop()
        })

        it('with different local and remote paths', async () => {
            const service = await newService()

            // Setup routes to fake a service
            const name = 'fakeservice'
            const path = '/' + String(Math.random()).substr(2)
            const rpath = '/' + String(Math.random()).substr(2)
            service.route(path, _ => path.substring(1))
            assert.notEqual(path, rpath)

            // Register the route as a service
            const url_self = 'http://127.0.0.1:' + port_orchestrator
            await register(
                service, {url: new URL(url_self), name: name, routes: [{path: path, remotePath: rpath}]})

            // Test service result
            const res1 = await request().get(`/service/${name}${path}`)
            const res2 = await request().get(`/service/${name}${rpath}`)
            assert.equal(res1.statusCode, 404)
            assert.equal(res2.statusCode, 200)
            assert.equal(res2.body, path.substr(1))

            service.stop()
        })
    })

    type _RegisterOptions = {name: string, path: string, method?: HttpMethod}
    async function newRegisteredService(opts: _RegisterOptions) {
        const service = await newService()
        const url_self = 'http://127.0.0.1:' + port_orchestrator
        await register(service, {url: new URL(url_self), name: opts.name, routes:
            [{path: opts.path, method: opts.method}]})
        return service
    }

    describe('request', () => {

        ['DELETE', 'GET', 'PATCH', 'POST', 'PUT'].forEach((method: HttpMethod) => {
            it(`${method}`, async () => {
                const opts = {path: '/' + Date.now(), name: 'fakeservice', method: method}
                const service = await newRegisteredService(opts)
                service.route(opts, _ => 'OK')

                const req = new HttpMessageBuilder()
                    .setMethod(opts.method).setPath(`/service/${opts.name}${opts.path}`).build()
                const res = await request().request(req)

                assert.equal(res.statusCode, 200)
                assert.equal(res.body, 'OK')

                service.stop()
            })
        })

        it('root path', async () => {
            const opts = {path: '/', name: 'fakeservice'}
            const service = await newRegisteredService(opts)
            service.route(opts.path, _ => 'Hello World')

            const res1 = await request().get(`/service/${opts.name}${opts.path}`)
            // const res2 = await request().get(`/service/${opts.name}`)

            assert.equal(res1.statusCode, 200)
            // assert.equal(res2.statusCode, 200)
            assert.equal(res1.body, 'Hello World')
            // assert.equal(res2.body, 'Hello World')

            service.stop()
        })

        it('service round robin', async () => {
            const opts = {path: '/' + Date.now(), name: 'fakeservice'}
            const service1 = await newRegisteredService(opts)
            const service2 = await newRegisteredService(opts)
            const service3 = await newRegisteredService(opts)

            service1.route(opts.path, _ => 'a')
            service2.route(opts.path, _ => 'b')
            service3.route(opts.path, _ => 'c')

            const res1 = await request().get(`/service/${opts.name}${opts.path}?ts=${Date.now()}`)
            orchestrator.clearCache()
            const res2 = await request().get(`/service/${opts.name}${opts.path}?ts=${Date.now()}`)
            orchestrator.clearCache()
            const res3 = await request().get(`/service/${opts.name}${opts.path}?ts=${Date.now()}`)
            orchestrator.clearCache()
            const res4 = await request().get(`/service/${opts.name}${opts.path}?ts=${Date.now()}`)

            assert.equal(res1.body, 'a')
            assert.equal(res2.body, 'b')
            assert.equal(res3.body, 'c')
            assert.equal(res4.body, 'a')

            service1.stop()
            service2.stop()
            service3.stop()
        })

        it('service deletion', async () => {
            const opts = {path: '/' + Date.now(), name: 'fakeservice'}
            const service1 = await newRegisteredService(opts)
            const service2 = await newRegisteredService(opts)
            const service3 = await newRegisteredService(opts)

            service1.route(opts.path, _ => 'a')
            service2.route(opts.path, _ => 'b')
            service3.route(opts.path, _ => 'c')

            const res1 = await request().get(`/service/${opts.name}${opts.path}?ts=${Date.now()}`)
            orchestrator.clearCache()
            service1.stop()
            const res2 = await request().get(`/service/${opts.name}${opts.path}?ts=${Date.now()}`)
            orchestrator.clearCache()
            service2.stop()
            const res3 = await request().get(`/service/${opts.name}${opts.path}?ts=${Date.now()}`)
            orchestrator.clearCache()
            service3.stop()
            const res4 = await request().get(`/service/${opts.name}${opts.path}?ts=${Date.now()}`)

            assert.equal(res1.body, 'a')
            assert.equal(res2.body, 'b')
            assert.equal(res3.body, 'c')
            assert.equal(res4.statusCode, 404)
        })

        it('orchestrator starts after register is called', async () => {
            orchestrator.stop()
            await new Promise(resolve => setTimeout(resolve, 1000))

            const opts = {path: '/' + Date.now(), name: 'fakeservice'}
            const service = await newRegisteredService(opts)
            service.route(opts.path, _ => 'Hello World')

            orchestrator.start(port_orchestrator)
            await new Promise(resolve => setTimeout(resolve, 8000))

            const res1 = await request().get(`/service/${opts.name}${opts.path}`)
            assert.equal(res1.statusCode, 200)
            assert.equal(res1.body, 'Hello World')

            service.stop()
        }).timeout(30000)

        it('orchestrator goes away and comes back', async () => {
            const opts = {path: '/' + Date.now(), name: 'fakeservice'}
            const service = await newRegisteredService(opts)
            service.route(opts.path, _ => 'Hello World')

            const res1 = await request().get(`/service/${opts.name}${opts.path}?t=1`)
            assert.equal(res1.statusCode, 200)
            assert.equal(res1.body, 'Hello World')

            orchestrator.stop()
            await new Promise(resolve => setTimeout(resolve, 1000))

            orchestrator.start(port_orchestrator)
            await new Promise(resolve => setTimeout(resolve, 8000))

            const res2 = await request().get(`/service/${opts.name}${opts.path}?t=2`)
            assert.equal(res2.statusCode, 200)
            assert.equal(res2.body, 'Hello World')

            service.stop()
        }).timeout(30000)

    })

    describe(`benchmark`, () => {

        function newRegisteredServicePool(opts: _RegisterOptions, num: number = 1):
                Promise<MicroService[]> {
            const microServicePool: Promise<MicroService>[] = []
            for (let i = 0; i < num; i++) {
                const service = newRegisteredService(opts)
                service.then(service =>
                    service.log = async (level: string, ...args: any[]) => null)
                microServicePool.push(service)
            }
            return Promise.all(microServicePool)
        }

        // Disable logging during benchmark
        let tmplog: (level: string, ...args: any[]) => Promise<string> = null
        before(() => {
            tmplog = orchestrator.log
            orchestrator.log = async (level: string, ...args: any[]) => null
        })
        after(() => {
            orchestrator.log = tmplog
        })

        // Define a helper function to time repetitions
        interface TimeLoopOptions {
            targetPerSecond?: number,
            iterationCount?: number,
            warmupCount?: number,
            batchSize?: number
        }
        const _timeLoopDefaults = {
            targetPerSecond: 20,
            iterationCount: 150,
            warmupCount: 50,
            batchSize: 8} as TimeLoopOptions
        async function timeLoop(func: () => Promise<any>, opts: TimeLoopOptions = {}) {
            opts = Object.assign({}, opts, _timeLoopDefaults)

            // Do not time the first `skip` iterations of the loop
            for (let i = 0; i < opts.warmupCount; i++) {
                await func()
            }

            // Start the clock and run the remaining iterations
            const start = Date.now()
            const awaitPool: Promise<any>[] = []
            for (let i = 0; i < opts.iterationCount; i++) {
                awaitPool.push(func())
                if (awaitPool.length >= opts.batchSize) {
                    await Promise.all(awaitPool)
                    awaitPool.splice(0, awaitPool.length)
                }
            }
            await Promise.all(awaitPool)

            // Compute the iterations per seconds and assert that we meet target
            const totaltime = Date.now() - start
            const persecond = (opts.iterationCount - opts.warmupCount) / (totaltime / 1000.0)
            const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`
            console.log(msg)
            assert(persecond > opts.targetPerSecond, msg)
        }

        it('random route', async () => {
            const r = request()
            const opts = {name: 'fakeservice', path: '/' + Date.now()}
            const services = await newRegisteredServicePool(opts)
            services.forEach(service => service.route(opts.path, _ => String(Date.now())))

            // Start test loop and stop services after done
            await timeLoop(() => r.get(
                `/service/${opts.name}${opts.path}?ts=${String(Math.random()).substr(2)}`))
            services.forEach(service => service.stop())

        }).timeout(20000)

        it('database read', async () => {
            const r = request()
            const opts = {name: 'fakeservice', path: '/' + Date.now()}
            const services = await newRegisteredServicePool(opts)
            services.forEach(service => service.route(opts.path,
                async req => await service.db.get(`SELECT * FROM log`)))

            // Start test loop and stop services after done
            await timeLoop(() => r.get(
                `/service/${opts.name}${opts.path}?ts=${String(Math.random()).substr(2)}`))
            services.forEach(service => service.stop())

        }).timeout(20000)

        it('large body', async () => {
            const r = request()
            const opts = {name: 'fakeservice', path: '/' + Date.now()}
            const services = await newRegisteredServicePool(opts)

            // Create large text
            let text = ''
            for (let i = 0; i < 4096; i++) {
                text += Math.random().toString(36).substr(2)
            }
            console.log(`Length of body: ${text.length} bytes`)
            services.forEach(service => service.route(opts.path, async req => text))

            // Start test loop and stop services after done
            await timeLoop(() => r.get(
                `/service/${opts.name}${opts.path}?ts=${String(Math.random()).substr(2)}`))
            services.forEach(service => service.stop())

        }).timeout(20000)

        it('cached', async () => {
            const r = request()
            const opts = {name: 'fakeservice', path: '/' + Date.now()}
            const services = await newRegisteredServicePool(opts)

            // Start test loop and stop services after done
            await timeLoop(() => r.get(
                `/service/${opts.name}${opts.path}`))
            services.forEach(service => service.stop())

        }).timeout(20000)

        it('request flooding', async () => {
            const r = request()
            const opts = {name: 'fakeservice', path: '/' + Date.now()}
            const services = await newRegisteredServicePool(opts)

            // Start test loop and stop services after done
            await timeLoop(() => r.get(
                `/service/${opts.name}${opts.path}`), {batchSize: 1000})
            services.forEach(service => service.stop())
        }).timeout(20000)
    })
});