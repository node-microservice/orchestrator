import { MicroService, RouteOptions, WebSocketRequest, WebSocketManager } from 'microservice'
export const REGISTER_PATH = '/register'
export const HEARTBEAT_PATH = '/_heartbeat'

export interface RemoteOptions extends RouteOptions {
    remotePath?: string     // path advertised as route in remote
}
export interface RegisterOptions {
    url: URL                // URL of the orchestrator
    name: string            // name advertised for this service
    routes: RemoteOptions[] // configuration for each route being registered
}

/**
 * Connect with a MicroService Orchestrator to broadcast the registered routes.
 *
 * @param service the service being registered, so we can add the heartbeat routes
 * @param options configuration for this service, including orchestrator, name, path, method...
 */
export async function register(service: MicroService, options: RegisterOptions,
            retryTimeout: number = 1000): Promise<void> {
    let retryAllowed = true

    // Method defaults to GET
    options.routes.forEach(route => route.method = route.method || 'GET')

    // Remote path falls back to local path
    options.routes.forEach(route => route.remotePath = route.remotePath || route.path)

    // Set up the heartbeat route
    const heartbeatRoute = service.route({path: HEARTBEAT_PATH, method: 'GET'}, _ =>
        Object({ timestamp: Date.now(), status: 'OK' }))

    const retry = () => {
        if (!retryAllowed) return
        setTimeout(() => {
            // Exponential back-off capped at 10 mins
            const waitTime = Math.min(10 * 60 * 1000, retryTimeout * 2)
            // Unset heartbeat route and try to register again
            service.unroute(heartbeatRoute)
            register(service, options, waitTime)
        }, retryTimeout)
    }

    // Create a websocket connection
    let conn: WebSocketRequest
    let manager: WebSocketManager
    try {
        conn = new WebSocketRequest(options.url)
        service.log('I', 'Establishing connection with orchestrator...')
        manager = await conn.manager
        service.log('I', 'Connection with orchestrator established')
    } catch (exc) {
        return retry()
    }

    // Listen to incoming messages in that connection and patch them through to our request handler
    service.router.addWebSocketListener(manager)

    // Make sure that all paths are prefixed with a slash
    options.routes.forEach(route => {
        if (!route.path.startsWith('/')) route.path = '/' + route.path
        if (!route.remotePath.startsWith('/')) route.remotePath = '/' + route.remotePath
    })

    // Closing the service also closes the registration
    const onstop = service.stop
    service.stop = () => {
        retryAllowed = false
        onstop.call(service)
        manager.close()
    }

    // Use established connection to post our registration at the orchestrator
    const response = await conn.post(
        REGISTER_PATH, Object.assign({}, options, {id: manager.id})).catch(_ => retry())

    // If registration failed, close the connection and retry
    if (response && response.statusCode !== 200) {
        manager.close()
        return retry()
    }

    // When the connection fails, re-try registration
    manager.once('close', retry)
}