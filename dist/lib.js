"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const microservice_1 = require("microservice");
exports.REGISTER_PATH = '/register';
exports.HEARTBEAT_PATH = '/_heartbeat';
/**
 * Connect with a MicroService Orchestrator to broadcast the registered routes.
 *
 * @param service the service being registered, so we can add the heartbeat routes
 * @param options configuration for this service, including orchestrator, name, path, method...
 */
function register(service, options, retryTimeout = 1000) {
    return __awaiter(this, void 0, void 0, function* () {
        let retryAllowed = true;
        // Method defaults to GET
        options.routes.forEach(route => route.method = route.method || 'GET');
        // Remote path falls back to local path
        options.routes.forEach(route => route.remotePath = route.remotePath || route.path);
        // Set up the heartbeat route
        const heartbeatRoute = service.route({ path: exports.HEARTBEAT_PATH, method: 'GET' }, _ => Object({ timestamp: Date.now(), status: 'OK' }));
        const retry = () => {
            if (!retryAllowed)
                return;
            setTimeout(() => {
                // Exponential back-off capped at 10 mins
                const waitTime = Math.min(10 * 60 * 1000, retryTimeout * 2);
                // Unset heartbeat route and try to register again
                service.unroute(heartbeatRoute);
                register(service, options, waitTime);
            }, retryTimeout);
        };
        // Create a websocket connection
        let conn;
        let manager;
        try {
            conn = new microservice_1.WebSocketRequest(options.url);
            service.log('I', 'Establishing connection with orchestrator...');
            manager = yield conn.manager;
            service.log('I', 'Connection with orchestrator established');
        }
        catch (exc) {
            return retry();
        }
        // Listen to incoming messages in that connection and patch them through to our request handler
        service.router.addWebSocketListener(manager);
        // Make sure that all paths are prefixed with a slash
        options.routes.forEach(route => {
            if (!route.path.startsWith('/'))
                route.path = '/' + route.path;
            if (!route.remotePath.startsWith('/'))
                route.remotePath = '/' + route.remotePath;
        });
        // Closing the service also closes the registration
        const onstop = service.stop;
        service.stop = () => {
            retryAllowed = false;
            onstop.call(service);
            manager.close();
        };
        // Use established connection to post our registration at the orchestrator
        const response = yield conn.post(exports.REGISTER_PATH, Object.assign({}, options, { id: manager.id })).catch(_ => retry());
        // If registration failed, close the connection and retry
        if (response && response.statusCode !== 200) {
            manager.close();
            return retry();
        }
        // When the connection fails, re-try registration
        manager.once('close', retry);
    });
}
exports.register = register;
