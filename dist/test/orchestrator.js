"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const microservice_1 = require("microservice");
const assert = require("assert");
const index_1 = require("../index");
const lib_1 = require("../lib");
describe('orquestrator', function () {
    let orchestrator;
    let request;
    const port_orchestrator = Math.floor(Math.random() * 8975) + 1024;
    before(() => __awaiter(this, void 0, void 0, function* () {
        orchestrator = yield index_1.main();
    }));
    beforeEach(() => __awaiter(this, void 0, void 0, function* () {
        orchestrator.start(port_orchestrator);
        request = () => new microservice_1.WebSocketRequest(orchestrator.server);
    }));
    afterEach(() => {
        orchestrator.stop();
        orchestrator.clearCache(true);
    });
    /**
     * Helper function used to create a new service using MicroService framework
     */
    function newService() {
        return __awaiter(this, void 0, void 0, function* () {
            const service = yield microservice_1.MicroService.create();
            const port_service = Math.floor(Math.random() * 8975) + 1024;
            service.start(port_service);
            return service;
        });
    }
    describe('heartbeat', () => {
        it('OK', () => __awaiter(this, void 0, void 0, function* () {
            const service = yield newService();
            // Setup routes to fake a service
            const name = 'fakeservice';
            const path = '/' + Date.now();
            service.route(path, _ => path.substring(1));
            // Register the route as a service
            const url_self = 'http://127.0.0.1:' + port_orchestrator;
            yield lib_1.register(service, { url: new URL(url_self), name: name, routes: [{ path: path }] });
            // Test service heartbeat
            const res = yield new microservice_1.HttpRequest(service.server).get(`${lib_1.HEARTBEAT_PATH}`);
            assert.equal(res.statusCode, 200);
            service.stop();
        }));
    });
    describe('register', () => {
        it('random path', () => __awaiter(this, void 0, void 0, function* () {
            const service = yield newService();
            // Setup routes to fake a service
            const name = 'fakeservice';
            const path = '/' + Date.now();
            service.route(path, _ => path.substring(1));
            // Register the route as a service
            const url_self = 'http://127.0.0.1:' + port_orchestrator;
            yield lib_1.register(service, { url: new URL(url_self), name: name, routes: [{ path: path }] });
            // Test service result
            const res = yield request().get(`/service/${name}${path}`);
            assert.equal(res.statusCode, 200);
            assert.equal(res.body, path.substr(1));
            service.stop();
        }));
        it('same path twice', () => __awaiter(this, void 0, void 0, function* () {
            const service = yield newService();
            // Setup routes to fake a service
            const name = 'fakeservice';
            const path = '/' + Date.now();
            const opts = service.route(path, _ => path.substring(1));
            // Register the route as a service
            const url_self = 'http://127.0.0.1:' + port_orchestrator;
            yield lib_1.register(service, { url: new URL(url_self), name: name, routes: [{ path: path }] });
            yield lib_1.register(service, { url: new URL(url_self), name: name, routes: [{ path: path }] });
            // Test service result
            const res = yield request().get(`/service/${name}${path}`);
            assert.equal(res.statusCode, 200);
            assert.equal(res.body, path.substr(1));
            service.stop();
        }));
        it('root path', () => __awaiter(this, void 0, void 0, function* () {
            const service = yield newService();
            // Setup routes to fake a service
            const name = 'fakeservice';
            const path = '/';
            service.route(path, _ => path.substring(1));
            // Register the route as a service
            const url_self = 'http://127.0.0.1:' + port_orchestrator;
            yield lib_1.register(service, { url: new URL(url_self), name: name, routes: [{ path: path }] });
            // Test service result
            const res = yield request().get(`/service/${name}${path}?${Date.now()}`);
            assert.equal(res.statusCode, 200);
            assert.equal(res.body, path.substr(1));
            service.stop();
        }));
        it('with different local and remote paths', () => __awaiter(this, void 0, void 0, function* () {
            const service = yield newService();
            // Setup routes to fake a service
            const name = 'fakeservice';
            const path = '/' + String(Math.random()).substr(2);
            const rpath = '/' + String(Math.random()).substr(2);
            service.route(path, _ => path.substring(1));
            assert.notEqual(path, rpath);
            // Register the route as a service
            const url_self = 'http://127.0.0.1:' + port_orchestrator;
            yield lib_1.register(service, { url: new URL(url_self), name: name, routes: [{ path: path, remotePath: rpath }] });
            // Test service result
            const res1 = yield request().get(`/service/${name}${path}`);
            const res2 = yield request().get(`/service/${name}${rpath}`);
            assert.equal(res1.statusCode, 404);
            assert.equal(res2.statusCode, 200);
            assert.equal(res2.body, path.substr(1));
            service.stop();
        }));
    });
    function newRegisteredService(opts) {
        return __awaiter(this, void 0, void 0, function* () {
            const service = yield newService();
            const url_self = 'http://127.0.0.1:' + port_orchestrator;
            yield lib_1.register(service, { url: new URL(url_self), name: opts.name, routes: [{ path: opts.path, method: opts.method }] });
            return service;
        });
    }
    describe('request', () => {
        ['DELETE', 'GET', 'PATCH', 'POST', 'PUT'].forEach((method) => {
            it(`${method}`, () => __awaiter(this, void 0, void 0, function* () {
                const opts = { path: '/' + Date.now(), name: 'fakeservice', method: method };
                const service = yield newRegisteredService(opts);
                service.route(opts, _ => 'OK');
                const req = new microservice_1.HttpMessageBuilder()
                    .setMethod(opts.method).setPath(`/service/${opts.name}${opts.path}`).build();
                const res = yield request().request(req);
                assert.equal(res.statusCode, 200);
                assert.equal(res.body, 'OK');
                service.stop();
            }));
        });
        it('root path', () => __awaiter(this, void 0, void 0, function* () {
            const opts = { path: '/', name: 'fakeservice' };
            const service = yield newRegisteredService(opts);
            service.route(opts.path, _ => 'Hello World');
            const res1 = yield request().get(`/service/${opts.name}${opts.path}`);
            // const res2 = await request().get(`/service/${opts.name}`)
            assert.equal(res1.statusCode, 200);
            // assert.equal(res2.statusCode, 200)
            assert.equal(res1.body, 'Hello World');
            // assert.equal(res2.body, 'Hello World')
            service.stop();
        }));
        it('service round robin', () => __awaiter(this, void 0, void 0, function* () {
            const opts = { path: '/' + Date.now(), name: 'fakeservice' };
            const service1 = yield newRegisteredService(opts);
            const service2 = yield newRegisteredService(opts);
            const service3 = yield newRegisteredService(opts);
            service1.route(opts.path, _ => 'a');
            service2.route(opts.path, _ => 'b');
            service3.route(opts.path, _ => 'c');
            const res1 = yield request().get(`/service/${opts.name}${opts.path}?ts=${Date.now()}`);
            orchestrator.clearCache();
            const res2 = yield request().get(`/service/${opts.name}${opts.path}?ts=${Date.now()}`);
            orchestrator.clearCache();
            const res3 = yield request().get(`/service/${opts.name}${opts.path}?ts=${Date.now()}`);
            orchestrator.clearCache();
            const res4 = yield request().get(`/service/${opts.name}${opts.path}?ts=${Date.now()}`);
            assert.equal(res1.body, 'a');
            assert.equal(res2.body, 'b');
            assert.equal(res3.body, 'c');
            assert.equal(res4.body, 'a');
            service1.stop();
            service2.stop();
            service3.stop();
        }));
        it('service deletion', () => __awaiter(this, void 0, void 0, function* () {
            const opts = { path: '/' + Date.now(), name: 'fakeservice' };
            const service1 = yield newRegisteredService(opts);
            const service2 = yield newRegisteredService(opts);
            const service3 = yield newRegisteredService(opts);
            service1.route(opts.path, _ => 'a');
            service2.route(opts.path, _ => 'b');
            service3.route(opts.path, _ => 'c');
            const res1 = yield request().get(`/service/${opts.name}${opts.path}?ts=${Date.now()}`);
            orchestrator.clearCache();
            service1.stop();
            const res2 = yield request().get(`/service/${opts.name}${opts.path}?ts=${Date.now()}`);
            orchestrator.clearCache();
            service2.stop();
            const res3 = yield request().get(`/service/${opts.name}${opts.path}?ts=${Date.now()}`);
            orchestrator.clearCache();
            service3.stop();
            const res4 = yield request().get(`/service/${opts.name}${opts.path}?ts=${Date.now()}`);
            assert.equal(res1.body, 'a');
            assert.equal(res2.body, 'b');
            assert.equal(res3.body, 'c');
            assert.equal(res4.statusCode, 404);
        }));
        it('orchestrator starts after register is called', () => __awaiter(this, void 0, void 0, function* () {
            orchestrator.stop();
            yield new Promise(resolve => setTimeout(resolve, 1000));
            const opts = { path: '/' + Date.now(), name: 'fakeservice' };
            const service = yield newRegisteredService(opts);
            service.route(opts.path, _ => 'Hello World');
            orchestrator.start(port_orchestrator);
            yield new Promise(resolve => setTimeout(resolve, 8000));
            const res1 = yield request().get(`/service/${opts.name}${opts.path}`);
            assert.equal(res1.statusCode, 200);
            assert.equal(res1.body, 'Hello World');
            service.stop();
        })).timeout(30000);
        it('orchestrator goes away and comes back', () => __awaiter(this, void 0, void 0, function* () {
            const opts = { path: '/' + Date.now(), name: 'fakeservice' };
            const service = yield newRegisteredService(opts);
            service.route(opts.path, _ => 'Hello World');
            const res1 = yield request().get(`/service/${opts.name}${opts.path}?t=1`);
            assert.equal(res1.statusCode, 200);
            assert.equal(res1.body, 'Hello World');
            orchestrator.stop();
            yield new Promise(resolve => setTimeout(resolve, 1000));
            orchestrator.start(port_orchestrator);
            yield new Promise(resolve => setTimeout(resolve, 8000));
            const res2 = yield request().get(`/service/${opts.name}${opts.path}?t=2`);
            assert.equal(res2.statusCode, 200);
            assert.equal(res2.body, 'Hello World');
            service.stop();
        })).timeout(30000);
    });
    describe(`benchmark`, () => {
        function newRegisteredServicePool(opts, num = 1) {
            const microServicePool = [];
            for (let i = 0; i < num; i++) {
                const service = newRegisteredService(opts);
                service.then(service => service.log = (level, ...args) => __awaiter(this, void 0, void 0, function* () { return null; }));
                microServicePool.push(service);
            }
            return Promise.all(microServicePool);
        }
        // Disable logging during benchmark
        let tmplog = null;
        before(() => {
            tmplog = orchestrator.log;
            orchestrator.log = (level, ...args) => __awaiter(this, void 0, void 0, function* () { return null; });
        });
        after(() => {
            orchestrator.log = tmplog;
        });
        const _timeLoopDefaults = {
            targetPerSecond: 20,
            iterationCount: 150,
            warmupCount: 50,
            batchSize: 8
        };
        function timeLoop(func, opts = {}) {
            return __awaiter(this, void 0, void 0, function* () {
                opts = Object.assign({}, opts, _timeLoopDefaults);
                // Do not time the first `skip` iterations of the loop
                for (let i = 0; i < opts.warmupCount; i++) {
                    yield func();
                }
                // Start the clock and run the remaining iterations
                const start = Date.now();
                const awaitPool = [];
                for (let i = 0; i < opts.iterationCount; i++) {
                    awaitPool.push(func());
                    if (awaitPool.length >= opts.batchSize) {
                        yield Promise.all(awaitPool);
                        awaitPool.splice(0, awaitPool.length);
                    }
                }
                yield Promise.all(awaitPool);
                // Compute the iterations per seconds and assert that we meet target
                const totaltime = Date.now() - start;
                const persecond = (opts.iterationCount - opts.warmupCount) / (totaltime / 1000.0);
                const msg = `Requests per second: ${persecond.toFixed(2)}. Total time: ${totaltime} ms`;
                console.log(msg);
                assert(persecond > opts.targetPerSecond, msg);
            });
        }
        it('random route', () => __awaiter(this, void 0, void 0, function* () {
            const r = request();
            const opts = { name: 'fakeservice', path: '/' + Date.now() };
            const services = yield newRegisteredServicePool(opts);
            services.forEach(service => service.route(opts.path, _ => String(Date.now())));
            // Start test loop and stop services after done
            yield timeLoop(() => r.get(`/service/${opts.name}${opts.path}?ts=${String(Math.random()).substr(2)}`));
            services.forEach(service => service.stop());
        })).timeout(20000);
        it('database read', () => __awaiter(this, void 0, void 0, function* () {
            const r = request();
            const opts = { name: 'fakeservice', path: '/' + Date.now() };
            const services = yield newRegisteredServicePool(opts);
            services.forEach(service => service.route(opts.path, (req) => __awaiter(this, void 0, void 0, function* () { return yield service.db.get(`SELECT * FROM log`); })));
            // Start test loop and stop services after done
            yield timeLoop(() => r.get(`/service/${opts.name}${opts.path}?ts=${String(Math.random()).substr(2)}`));
            services.forEach(service => service.stop());
        })).timeout(20000);
        it('large body', () => __awaiter(this, void 0, void 0, function* () {
            const r = request();
            const opts = { name: 'fakeservice', path: '/' + Date.now() };
            const services = yield newRegisteredServicePool(opts);
            // Create large text
            let text = '';
            for (let i = 0; i < 4096; i++) {
                text += Math.random().toString(36).substr(2);
            }
            console.log(`Length of body: ${text.length} bytes`);
            services.forEach(service => service.route(opts.path, (req) => __awaiter(this, void 0, void 0, function* () { return text; })));
            // Start test loop and stop services after done
            yield timeLoop(() => r.get(`/service/${opts.name}${opts.path}?ts=${String(Math.random()).substr(2)}`));
            services.forEach(service => service.stop());
        })).timeout(20000);
        it('cached', () => __awaiter(this, void 0, void 0, function* () {
            const r = request();
            const opts = { name: 'fakeservice', path: '/' + Date.now() };
            const services = yield newRegisteredServicePool(opts);
            // Start test loop and stop services after done
            yield timeLoop(() => r.get(`/service/${opts.name}${opts.path}`));
            services.forEach(service => service.stop());
        })).timeout(20000);
        it('request flooding', () => __awaiter(this, void 0, void 0, function* () {
            const r = request();
            const opts = { name: 'fakeservice', path: '/' + Date.now() };
            const services = yield newRegisteredServicePool(opts);
            // Start test loop and stop services after done
            yield timeLoop(() => r.get(`/service/${opts.name}${opts.path}`), { batchSize: 1000 });
            services.forEach(service => service.stop());
        })).timeout(20000);
    });
});
