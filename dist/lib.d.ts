import { MicroService, RouteOptions } from 'microservice';
export declare const REGISTER_PATH = "/register";
export declare const HEARTBEAT_PATH = "/_heartbeat";
export interface RemoteOptions extends RouteOptions {
    remotePath?: string;
}
export interface RegisterOptions {
    url: URL;
    name: string;
    routes: RemoteOptions[];
}
/**
 * Connect with a MicroService Orchestrator to broadcast the registered routes.
 *
 * @param service the service being registered, so we can add the heartbeat routes
 * @param options configuration for this service, including orchestrator, name, path, method...
 */
export declare function register(service: MicroService, options: RegisterOptions, retryTimeout?: number): Promise<void>;
