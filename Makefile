.PHONY: all
all: usage

usage:
	@echo "Docker Build and Push"
	@echo "Usage:"
	@echo "  make build: Build the Docker image"
	@echo "  make run: Runs the Docker image"
	@echo "  make clean: Kills and deletes all instances of the Docker image"
	@echo "  make push: Push image to registry"
	@echo ""
	@echo "Change registry and image name in Makefile before build and push."

build:
	docker-compose build

run:
	docker-compose up -d

clean:
	docker-compose rm -sf
	docker system prune -af

push: build
	docker-compose push
