FROM node:10
LABEL MAINTANER omtinez <omtinez@gmail.com>

# Root certificates update and deps
RUN apt-get update && \
    apt-get install -yq git wget ca-certificates && \
    update-ca-certificates

# Make sure that the cache directories exist
RUN mkdir -p /var/cache

# Get the code into the container
COPY . /src
WORKDIR /src

# Install dependencies and build service
RUN yarn install
RUN yarn build

# Test service to ensure we built it correctly
RUN yarn test -- --grep skipci --invert

ENTRYPOINT ["yarn", "start"]