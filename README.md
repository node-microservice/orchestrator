# MicroService Orchestrator

Enables scaling of microservices by providing a registration mechanism and round-robin scheduling of request handling.

## Installation

Installing this package globally also installs a SystemD template unit file. To start the SystemD service then, run the following commands as sudo:

    npm install -g gitlab:node-microservice/orchestrator
    systemctl daemon-reload
    systemctl enable microservice-orchestrator@1234
    systemctl start microservice-orchestrator@1234

Substitute 1234 with the port under which you want to run the microservice orchestrator.

## Usage

Interacting with the microservice orchestrator via HTTP requests is not recommended. Instead, you should be using the Microservice.register() API from the base [MicroService](https://gitlab.com/node-microservice/microservice) class. Refer to that project if you want details about the internals. For a simple usage example, refer to the reference implementation in the [template](https://gitlab.com/node-microservice/template) which automatically registers with the orchestrator when the flag `orchestrator <url>` is passed.

Note that registration is done as part of the maintenance loop every minute, this is because:
1. Trying to register an already registered service causes no harm.
2. If the service fails to respond to a request to the /status endpoint, it will be removed from the orchestrator. As a mechanism to guard against transient failures, periodic registration is used.

## Benchmarks

The following are results from benchmark testing on a Macbook Pro 2015:

| Test            | Requests per second with orchestrator | Requests per second without orchestrator |
| --------------- | ------------------------------------- | ---------------------------------------- |
| Cached requests | 888.89                                | 1141.55                                  |
| `Math.random()` | 784.93                                | 879.51                                   |
| 4xx status      | 851.06                                | 696.86                                   |
| /status         | 593.47                                | N/A                                      |

The network requests are made in series, waiting for one to finish before dispatching the next. To run your own benchmarks, simply run: `npm run benchmark`.
