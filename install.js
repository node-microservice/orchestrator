'use strict';

const fs = require('fs');
const path = require('path');
const pckg = require('./package.json');
const args = require('minimist')(process.argv.slice(1));

// Installs this microservice as a systemd service, requires root

// Check if package was installed with global flag
if (!args.output && process.env.npm_config_argv &&
    JSON.parse(process.env.npm_config_argv).cooked.indexOf('--systemd') === -1) {
    console.log('Skipping SystemD service installation because --systemd flag was not used');
    process.exit(0);
}

// Ensure we are running as root
if (process.getuid() !== 0) {
    console.error('Installation failed!');
    console.error('Installation of microservice SystemD service must be done by root. Try running:');
    console.error('> sudo npm install --systemd <package> --unsafe-perm');
    process.exit(1);
}

// Read the service template and replace placeholders
const service = fs.readFileSync('systemd/microservice.service').toString()
    .replace(/__project_description__/g, pckg.description)
    .replace(/__project_name__/g, pckg.name)
    .replace(/__install_dir__/g, __dirname);

// Write it into provided location or fall back to default
const output = args.output || (`/etc/systemd/system/${pckg.name}@.service`);
console.log('Writing service unit file to ' + output);
fs.writeFileSync(output, service);

// Write environment file, overwrite if it exists
const envdir = '/etc/microservice'
fs.mkdirSync(envdir)
const myenv = path.join(envdir, `${pckg.name}.env`);
const myenv_content = fs.readFileSync('systemd/microservice.env').toString();
console.log('Writing environment file to ' + myenv);
fs.writeFileSync(myenv, myenv_content);
